clear all
tic
load('megInfo.mat'); % load participant information
MAIN_DIR = ''; % Directory with source reconstructed data in brainstorm format
Epoch = 500; % epoch length in time points
c = 1;
NUM_ITER = 100; % Number of surrogates

for ii = 1:100;
    SUB_ID = megInfo(ii,1);
    
    try
    file = dir(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\time*'));
    load(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\',file.name));
    TF = squeeze(TF);
    
    for jj = 1:68;
        TEMP = TF(jj,:);
        alpha = TEMP(Freqs< 12 & Freqs>8);
        f_alpha = Freqs(Freqs<12 & Freqs>8);
        paf(jj) = f_alpha(alpha == max(alpha));
    end
    
    PAF(c) = mean(paf,2);
    age(c) = megInfo(ii,2);
    
    file_data = dir(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\matrix*'));
    
    load(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\',file_data.name),'Value');
    
    WIN_NUMBER = 1;
    
    
    for win = 1:Epoch:size(Value,2)-Epoch; % Epoching
        
        DATA = Value(:,win:win+Epoch);    
    
        filt_data = ft_preproc_bandpassfilter(DATA,100,[PAF(c)+2 PAF(c)-2],[],'fir');
        phase_sig(WIN_NUMBER,:,:) = angle(hilbert(filt_data'));       
        
        WIN_NUMBER = WIN_NUMBER + 1;
        
    end
    
    NUM_WIN = size(phase_sig,1);
    plv = zeros(NUM_WIN,68,68);
    
    for n = 1:NUM_ITER
        for jjj = 1:68;
            for kkk = 1:68;
            
                plv(:,jjj,kkk) = abs(mean(squeeze(exp(1i*(phase_sig(randperm(NUM_WIN),:,jjj) - phase_sig(randperm(NUM_WIN),:,kkk)))),2));
                
            end
        end
        
        % diagnalize elements to 1.
        for lll = 1:NUM_WIN;
            for mmm = 1:68;
                plv(lll,mmm,mmm) = 1;
            end
        end
        
        
        PLV(n) = mean(plv(:));
    end
        
    PLV_FINAL(c,:) = PLV;
    
    c = c+1;
  

    catch
        continue
    end

end

toc

        
    