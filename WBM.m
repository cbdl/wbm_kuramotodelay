 
clear all
tic % setup timer
parpool(80); % setup parallel pool
load('AAL_matrices.mat'); % import connectivity 
conn = C/max(C(:)); % normalize connectivity
dist = D/1000; % cortico-cortical distances in meters

t = 100; % duration of simulation in seconds
dt = 0.001; % simulation step in seconds
tspan = 0:dt:t; % time steps
N = size(conn,1); % number of oscillators
sigma= sqrt(dt); % scale noise
noise = 3; % noise ampliude

% Assign natural frequencies according to Gollo et. al.
% Define frequency range
a = 12; % Highest freq
b = 8; % Lowest freq


% assign natural frequencies to oscillators
for n = 1:N;
    S(n,1) = sum(conn(n,:));
end

for n = 1:N;
    Omega(n,1) = a - (a-b)*(((S(n,1) - min(S))/(min(S)-max(S)))^2);
end

Omega = Omega *dt* 2*pi; 



cv = 1:0.5:30; % range for conduction velocity in m/s

G = 800:15:2000; % range for global coupling


G_ = G*(dt/N); % scale connectivity

% initialize arrays to save time
PLV= zeros(length(G),length(cv));
PAF = zeros(length(G),length(cv),N);
ord_param = zeros(length(G),length(cv));
mtsb = zeros(length(G),length(cv));



parfor gg = 1:length(G);
    
    temp_plv = zeros(length(cv),1);
    temp_PAF = zeros(length(cv),N);
    temp_ord_param = zeros(length(cv),1);
    temp_mtsb = zeros(length(cv),1);
    
    for delay = 1:length(cv);
        Theta = 1000*randn(length(tspan),N); % initiate random phases
        delays = round(dist/cv(delay),3)*(1/dt); % ensure integers
        max_d = max(delays(:));

        for tt= max_d+1:length(tspan)-1;
            for n = 1:N
                Theta(tt+1,n) =  Omega(n) + G_(gg)*sum(conn(n,:).*sin(Theta(sub2ind(size(Theta),tt-delays(n,:),1:1:size(Theta,2))) - Theta(tt,n))) + noise*sigma*randn + Theta(tt,n); % Kuramoto ODE
            end
        end
        Theta = Theta';
        
        signal = cos(Theta(:,tspan>10)); % generate cosine time series
        frq = 0:0.01:20; % frequency range for power spectra 
        [pxx,frq] = pwelch(signal',[],[],frq,1000); % compute power spectra
        [val, loc] = max(pxx);
        TEMP_PAF = frq(loc); % estimate PAF
       
        
        r=abs(mean(exp(1i*Theta),1)); % order parameter
        

        plv = zeros(68,68); % initialize phase locking 
        
         % estimate phase locking between all source pairs
        for ii=1:68;
            for jj = 1:68;
                plv(ii,jj) = abs(mean(exp(1i*(Theta(ii,tspan>10) - Theta(jj,tspan>10)))));
            end
        end
    	
        
        temp_PAF(delay,:) = TEMP_PAF;
        temp_plv(delay) = mean(plv(:));
        temp_ord_param(delay) = mean(r(tspan > 10));
        temp_mtsb(delay) = std(r(tspan > 10)); % metastability index


    end
    
    PLV(gg,:) = temp_plv;
    PAF(gg,:,:) = temp_PAF;    
    ord_param(gg,:) = temp_ord_param;
    mtsb(gg,:) = temp_mtsb;
    
    
    
end

t_run = toc;


save('Output','PLV','PAF','ord_param','mtsb','t_run','noise');





