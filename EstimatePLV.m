clear all
tic
load('megInfo.mat'); % load participant information
MAIN_DIR = ''; % Directory with source reconstructed data in brainstorm format

Epoch = 500; % epoch length in time points
c = 1;

for ii = 1:650;
    SUB_ID = megInfo(ii,1);
    
    try
    file = dir(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\time*'));
    load(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\',file.name));
    TF = squeeze(TF);
    
    
    for jj = 1:68;
        TEMP = TF(jj,:);
        alpha = TEMP(Freqs< 12 & Freqs>8);
        f_alpha = Freqs(Freqs<12 & Freqs>8);
        paf(jj) = f_alpha(alpha == max(alpha));
    end
    
    PAF(c) = mean(paf,2);
    age(c) = megInfo(ii,2);
    
    file_data = dir(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\matrix*'));
    
    load(strcat(MAIN_DIR,num2str(SUB_ID),'\@rawtransdef_mf2pt2_rest_raw_resample_bl_detrend_band\',file_data.name),'Value');
    
    WIN_NUMBER = 1;
    PLI_epoch = [];
    for win = 1:Epoch:size(Value,2)-Epoch;
        
        DATA = Value(:,win:win+Epoch);  % Epoch  
    
        filt_data = ft_preproc_bandpassfilter(DATA,100,[10 6],[],'fir'); % bandpass signal
        phase_sig = angle(hilbert(filt_data')); % extract phase
        plv = zeros(68,68); % initialize 


        for jjj = 1:68;
        
            for kkk = 1:68;
               
               plv(jjj,kkk) = abs(mean(exp(1i*(phase_sig(:,jjj) - phase_sig(:,kkk))),1)); % PLV function
               
            end
        end
        
        PLI_epoch(WIN_NUMBER) = mean(plv(:)); % mean PLI across ROIs
        WIN_NUMBER = WIN_NUMBER +1;
    end
    
    PLI(c) = mean(PLI_epoch);
    c = c+1;

    catch
        continue
    end

end

toc

         
    